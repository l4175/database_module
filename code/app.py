from fastapi import FastAPI
from routes.module_two import ModuleTwo
from config.openapi import tags_metadata
from starlette.middleware.cors import CORSMiddleware

app = FastAPI(
    title="Module Two API",
    description="This is a database module REST API using python and mysql",
    version="0.0.1",
    openapi_tags=tags_metadata,
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


app.include_router(ModuleTwo)


# uvicorn.run(app, host="127.0.0.1", port=8001)


# from fastapi.middleware.cors import CORSMiddleware
# from supertokens_fastapi import get_cors_allowed_headers
# from fastapi import FastAPI

# app = FastAPI()
# app.add_middleware(
#     CORSMiddleware,
#     allow_origins=[
#         "http://127.0.0.1:8080"
#     ],
#     allow_credentials=True,
#     allow_methods=["*"],
#     allow_headers=["Content-Type"] + get_cors_allowed_headers(),
# )
