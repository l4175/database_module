from pydantic import BaseModel
from typing import Optional


class User(BaseModel):
    id: Optional[str]
    name: str


class Audio(BaseModel):
    id: Optional[int]
    name: str
    id_session: int
    link: str
    duration: str


class Session(BaseModel):
    id: Optional[int]
    name: str


class Module_Two(BaseModel):
    id: Optional[int]
    minute_initial: str
    minute_final: str
    id_audio: int
    text_intial: str
    corrected_intial: str
    text_final: str
    id_session: int
    FK_u_asignado: int
    to_correct: bool



