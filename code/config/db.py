# -*- coding: utf-8 -*-
from click import password_option
from sqlalchemy import create_engine, MetaData

import os
import site
from dotenv import load_dotenv

load_dotenv()
db_name = os.getenv("DB_NAME")
load_dotenv()
user_name = os.getenv("DB_USER")
load_dotenv()
password = os.getenv("DB_PASSWD")
load_dotenv()
host = os.getenv("DB_PORT")


# DATABASE = "mysql+pymysql://sammy:password@localhost:3306/module_2"
# DATABASE = 'mysql+pymysql://'+str(user_name)+':'+str(password)+'@localhost:'+str(host)+'/'+str(db_name)+'?charset=utf8'

DATABASE = "mysql+pymysql://%s:%s@localhost:%s/%s?charset=utf8" % (
    user_name,
    password,
    host,
    db_name,
)

engine = create_engine(DATABASE, encoding="utf-8", echo=True)

meta = MetaData()

conn = engine.connect()
