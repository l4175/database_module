CREATE TABLE users (
    id INT NOT NULL AUTO_INCREMENT,
    name_users VARCHAR(255),
    PRIMARY KEY (id)
    
);

CREATE TABLE audio (
    id INT NOT NULL AUTO_INCREMENT,
    name_audio VARCHAR(255),
    id_session INT,
    link VARCHAR(255),
    duration VARCHAR(255),
    PRIMARY KEY (id),
    FOREIGN KEY (id_session) REFERENCES session1(id)
);

CREATE TABLE session1 (
    id INT NOT NULL AUTO_INCREMENT,
    name_session VARCHAR(255),
    PRIMARY KEY (id)
    
);

CREATE TABLE module_two (
    id INT NOT NULL AUTO_INCREMENT,
    minute_initial VARCHAR(255),
    minute_final VARCHAR(255),
    id_audio INT,
    text_intial TEXT,
    corrected_intial TEXT,
    text_final TEXT,
    id_session INT,
    FK_u_asignado INT,
    to_correct boolean DEFAULT true,
    PRIMARY KEY (id),
    FOREIGN KEY (id_audio) REFERENCES audio(id),
    FOREIGN KEY (id_session) REFERENCES session1(id),
    FOREIGN KEY (FK_u_asignado) REFERENCES users(id)
);