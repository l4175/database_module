
from fastapi import APIRouter, Response, status
from config.db import conn
from models.models import module_two, users, audio, session
from schemas.module_schemas import (
    Module_Two,
    Session,
    User,
    Audio,
    Session,
)
from cryptography.fernet import Fernet
from starlette.status import (
    HTTP_204_NO_CONTENT,
    HTTP_100_CONTINUE,
    HTTP_505_HTTP_VERSION_NOT_SUPPORTED,
    HTTP_400_BAD_REQUEST,
)
from typing import List
from sqlalchemy import func, select

key = Fernet.generate_key()
f = Fernet(key)
ModuleTwo = APIRouter()



@ModuleTwo.post(
    "/users", tags=["users"], response_model=User, description="Create a new user"
)
def create_user(user: User):
    new_user = {"name": user.name}
    result = conn.execute(users.insert().values(new_user))
    return conn.execute(users.select().where(users.c.id == result.lastrowid)).first()


@ModuleTwo.post(
    "/audios", tags=["audios"], response_model=Audio, description="Create a new audio"
)
def create_audio(user: Audio):
    new_user = {
        "name": user.name,
        "id_session": user.id_session,
        "Link": user.link,
        "duration": user.duration,
    }
    result = conn.execute(audio.insert().values(new_user))
    return conn.execute(audio.select().where(audio.c.id == result.lastrowid)).first()


@ModuleTwo.post(
    "/sessions",
    tags=["sessions"],
    response_model=Audio,
    description="Create a new session",
)
def create_session(user: Session):
    new_user = {"name": user.name}
    result = conn.execute(session.insert().values(new_user))
    return conn.execute(
        session.select().where(session.c.id == result.lastrowid)
    ).first()


@ModuleTwo.post(
    "/new-transcription",
    tags=["New Transcription"],
    response_model=Module_Two,
    description="Create a new transcription",
)
def create_transcription(user: Module_Two):
    new_user = {
        "minute_initial": user.minute_initial,
        "minute_final": user.minute_final,
        "id_audio": user.id_audio,
        "text_intial": user.text_intial,
        "corrected_intial": user.corrected_intial,
        "text_final": user.text_final,
        "id_session": user.id_session,
        "FK_u_asignado": user.FK_u_asignado,
    }
    result = conn.execute(module_two.insert().values(new_user))
    return conn.execute(
        session.select().where(module_two.c.id == result.lastrowid)
    ).first()


@ModuleTwo.get("/")
def helloworld():
    return "Hello! This is a Module API rest to create database of 'Licenciamiento de Cámara Project."
